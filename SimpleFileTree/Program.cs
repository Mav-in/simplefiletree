﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace SimpleFileTree
{
    /// <summary>
    /// Базовый класс элемента
    /// </summary>
    public class Item
    {
        /// <summary>
        /// Наименование элемента
        /// </summary>
        /// <permission cref="System.Security.PermissionSet">Свойство доступно для чтения</permission>
        /// <remarks>
        /// «TODO Для примера»
        /// </remarks>
        public string Name { get; /*private*/ set; }
        /// <summary>
        /// Размер элемента
        /// </summary>
        /// <permission cref="System.Security.PermissionSet">Свойство доступно для чтения, установка доступна только в наследниках</permission>
        public uint Size { get; protected set; }
        /// <summary>
        /// Установка размера элемента
        /// </summary>
        /// <typeparam name="uint"></typeparam>
        /// <param name="_size">Размер элемента</param>
        protected void SetSize(uint _size)
        {
            Size = _size;
        }
    }

    /// <summary>
    /// Класс файла от базового класса элемента
    /// </summary>
    public class FileItem : Item
    {
        /// <summary>
        /// Размер файла
        /// </summary>
        /// <permission cref="System.Security.PermissionSet">Свойство доступно для чтения</permission>
        /// <remarks>
        /// «TODO Для примера»
        /// </remarks>
        public new uint Size { get; /*private*/ set; }
    }

    /// <summary>
    /// Класс корневого каталога от базового класса элемента
    /// </summary>
    sealed public class FolderItem : Item
    {
        /// <summary>
        /// Коллекция каталога
        /// </summary>
        public Collection<Item> SubItems { get; set; }
        /// <summary>
        /// Добавление элемента
        /// </summary>
        /// <typeparam name="Item"></typeparam>
        /// <param name="_item">Элемент</param>
        public void AddItem(Item _item)
        {
            if (SubItems.Any(p => p.Name.Equals(_item.Name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new Exception("Дубликат наименования.");
            }
            SubItems.Add(_item);
            this.RefreshSize(SubItems);
            SetSize(Size + _item.Size);
        }
        /// <summary>
        /// Получение размера элемента
        /// </summary>
        /// <typeparam name="Item"></typeparam>
        /// <param name="_item">Элемент</param>
        /// <returns>Размер элемента</returns>
        public uint GetSize(Item _item)
        {
            this.RefreshSize(SubItems);
            return _item.Size;
        }
        /// <summary>
        /// Обновление размера каталогов
        /// </summary>
        /// <typeparam name="IEnumerable<Item>"></typeparam>
        /// <param name="Items">Элемент</param>
        /// <returns>Размер элемента</returns>
        public uint RefreshSize(IEnumerable<Item> Items)
        {
            uint _size = 0;
            uint _size_folder = 0;
            foreach (var Item in Items)
            {
                var folder = Item as FolderItem;
                if (folder != null)
                {
                    _size_folder += folder.RefreshSize(folder.SubItems);
                    folder.SetSize(_size_folder);
                }
                var file = Item as FileItem;
                if (file != null)
                    _size += file.Size;
                Size = _size + _size_folder;
            }
            return Size;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //TODO Для примера
            Collection<Item> Items = new Collection<Item>
            {
                new FolderItem
                {
                    Name = "f1",
                    SubItems = new Collection<Item>
                    {
                        new FileItem { Name = "1", Size = 1 },
                        new FileItem { Name = "2", Size = 10 },
                        new FolderItem
                        {
                            Name = "f2",
                            SubItems = new Collection<Item>
                            {
                                new FileItem { Name = "3", Size = 100 },
                                new FileItem { Name = "4", Size = 1000 },
                            }
                        },
                        new FolderItem
                        {
                            Name = "f3",
                            SubItems = new Collection<Item>
                            {
                                new FileItem { Name = "5", Size = 10000 },
                                new FileItem { Name = "6", Size = 100000 },
                            }
                        }
                    }
                },
                new FileItem { Name = "7", Size = 1000000 },
            };
            FolderItem root = new FolderItem();
            root.SubItems = Items;

            FileItem file = new FileItem { Name = "8", Size = 10000000 };
            root.AddItem(file);
            Console.ReadKey();
        }
    }
}
